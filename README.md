# cli-bqueue


Permet l'installation d'une commande cli : bqueue pour utiliser une queue bullmq

## prérequis
```
npm config set @meteo-routes-public:registry https://gitlab.com/api/v4/packages/npm/
```

## installation
```
npm i -g @meteo-routes-public:cli-bqueue
```


## Usage
Depuis la racine du projet bullmq (contenant le .env) :
```
bqueue // lance arena pour acceder à la queue
bqueue --clean 4 // clean completed job plus vieux que 4 jours
bqueue --reset  // WARNING : reset la queue completement
```

NB : le .env doit avoir soit les variables REDIS_DSN




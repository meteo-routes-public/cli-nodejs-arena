#!/usr/bin/env node
const readline = require("readline");
require("dotenv").config({ path: process.cwd() + "/.env" });

const confirmAndExecute = (question, action) => {
	return new Promise((resolve) => {
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
		});

		rl.question(question, (answer) => {
			if (answer.toLowerCase() === "y") {
				action();
			} else {
				console.log("Cancelled.");
			}
			rl.close();
			resolve();
		});
	});
};

(async () => {
	const { hideBin } = require("yargs/helpers");
	const argv = require("yargs/yargs")(hideBin(process.argv)).argv;

	if (argv.clean) {
		await confirmAndExecute("Are you sure you want to clean? (y/N) ", async () => {
			const keep = argv.clean && typeof argv.clean !== "boolean" && !isNaN(Number(argv.clean)) ? argv.clean : undefined;
			await require("./clean")(keep);
		});
	}

	if (argv.reset) {
		await confirmAndExecute("Are you sure you want to reset? (y/N) ", async () => {
			await require("./reset")();
		});
	}

	if (!argv.clean && !argv.reset) {
		require("./arena")(argv.port || undefined);
	}
})();

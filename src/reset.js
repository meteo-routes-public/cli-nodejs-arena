module.exports = async () => {
    const { Queue } = require('bullmq');
		const { dsn } = require("@meteo-routes-public/lib-nodejs-common");
		const connection = dsn('redis', process.env.BQUEUE_DSN);
		const queue = new Queue(connection.queue, { connection });
		
    await queue.obliterate({force: true});
    process.exit();
};

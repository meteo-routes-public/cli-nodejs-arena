const { dsn, log } = require("@meteo-routes-public/lib-nodejs-common");

module.exports = async (keep = 7) => {
	try {
		process.env.TZ = "UTC";
		const { Queue } = require("bullmq");
		const connection = dsn("redis", process.env.BQUEUE_DSN);
		const queue = new Queue(connection.queue, { connection });
		await queue.clean(3600 * 24 * keep * 1000, -1, "completed");

		process.exit();
	} catch (err) {
		log.dumpErr(err);
		process.exit(1);
	}
};

module.exports = (port = 4567) => {
	const Arena = require("bull-arena");
	const express = require("express");
	const router = express.Router();
	const { dsn, log } = require("@meteo-routes-public/lib-nodejs-common");
	const redis = dsn("redis", process.env.BQUEUE_DSN);
		
	const queues = [];
	queues.push({
		redis,
		name: redis.queue,
		hostId: `${redis.host}:${redis.port}`,
		type: "bullmq",
	});

	const arena = Arena(
		{
			BullMQ: require("bullmq").Queue,
			queues,
		},
		{
			port,
		}
	);
	router.use("/", arena);
	console.log(`[Arena Bullmq - ${redis.queue}]  http://127.0.0.1:${port}`);
};
